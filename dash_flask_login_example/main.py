import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd


df = pd.DataFrame({
    "Fruit": ["Apples", "Oranges", "Bananas", "Apples", "Oranges", "Bananas"],
    "Amount": [4, 1, 2, 2, 4, 5],
    "City": ["SF", "SF", "SF", "Montreal", "Montreal", "Montreal"]
})

fig = px.bar(df, x="Fruit", y="Amount", color="City", barmode="group")

def make_layout(username):
    return dbc.Container(
        dbc.Row(
            dbc.Col(
                [
                    html.P(f"Hello {username}, here is a graph for you."),
                    dcc.Graph(
                        id='example-graph',
                        figure=fig
                    )
                ],
                width=12
            ),
            className="mt-3"
        )
    )
