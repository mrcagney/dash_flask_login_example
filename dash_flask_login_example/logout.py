import dash_html_components as html
import dash_bootstrap_components as dbc


layout = dbc.Container(
    dbc.Row(
        dbc.Col(
            html.P(
                [
                    "Logged out. ",
                    html.A("Log in again?", href="/login")
                ],
            ),
        ),
        className="mt-3 pl-0",
    )
)
