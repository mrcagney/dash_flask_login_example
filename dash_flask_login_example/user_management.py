import sqlalchemy as sa
import sqlalchemy.sql as sas
import flask_sqlalchemy as fsa
import werkzeug.security as ws
import click

import settings as st


engine = sa.create_engine(st.config.SQLALCHEMY_DATABASE_URI)
db = fsa.SQLAlchemy()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))


user_table = sa.Table('user', User.metadata)


# Add a command line interface
@click.group()
def cli():
    """
    A basic set of command for managing users in the database.
    """
    pass

@cli.command()
def create_user_table():
    """
    Create the User table in the database.
    Only need to do this once.
    """
    User.metadata.create_all(engine)
    click.echo("User table created")


@cli.command()
@click.argument("username")
@click.argument("password")
@click.argument("email")
def add_user(username: str, password: str, email: str) -> None:
    """
    Add a user with the given username, password, and email address to the database.
    """
    hashed_password = ws.generate_password_hash(password, method='sha256')

    ins = user_table.insert().values(
        username=username, email=email, password=hashed_password)

    conn = engine.connect()
    conn.execute(ins)
    conn.close()

    click.echo(f"Added user {username}")


@cli.command()
@click.argument("username")
def remove_user(username: str) -> None:
    """
    Remove the user with the given username from the database.
    """
    delete = user_table.delete().where(user_table.c.username == username)

    conn = engine.connect()
    conn.execute(delete)
    conn.close()

    click.echo(f"Removed user {username}")


@cli.command()
def show_users() -> str:
    """
    Show the users (username, email address) registered in the database.
    """
    select_st = sas.select([user_table.c.username, user_table.c.email])

    conn = engine.connect()
    rs = conn.execute(select_st)

    for row in rs:
        print(row)

    conn.close()


if __name__ == '__main__':
    cli()