import dash_html_components as html
from dash import dependencies as dd
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import flask_login as fl

import settings as st
from app import app
import main, login, logout


server = app.server
app.layout = html.Div(
    [
        dcc.Location(id="url", refresh=False),
        dbc.NavbarSimple(
            brand="Dash Flask Login Example",
            color="dark",
            dark=True,
            className="justify-content-between",
            id="nav",
        ),
        # Page content set by URL (see callback :func:`display_page()`)
        html.Div(id="page-content"),
    ]
)


@app.callback(
    dd.Output('nav', 'children'),
    [dd.Input('page-content', 'children')]
)
def set_logout_link(input1):
    if fl.current_user.is_authenticated:
        return dbc.NavLink(f"Logout {fl.current_user.username}", href="/logout")


@app.callback(
    dd.Output("page-content", "children"), [dd.Input("url", "pathname")],
)
def display_page(pathname):
    """
    Display the page corresponding to the given URL.
    """
    if not fl.current_user.is_authenticated or pathname == "/login":
        result = login.layout
    elif pathname == "/":
        result = main.make_layout(fl.current_user.username)
    elif pathname == '/logout':
        if fl.current_user.is_authenticated:
            fl.logout_user()
        result = logout.layout
    else:
        result = html.Div("Whoops, that's an error!")

    return result


if __name__ == "__main__":
    app.run_server(debug=st.config.DEBUG, port=8051)
