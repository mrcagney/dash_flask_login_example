import flask_login as fl
import dash
import dash_bootstrap_components as dbc

import settings as st
import user_management as um


# Initialize app
app = dash.Dash(
    __name__,
    external_stylesheets=[
        dbc.themes.BOOTSTRAP,
    ],
    url_base_pathname="/",
    suppress_callback_exceptions=True,
    title="Dash Flask Login Example",
)
server = app.server
server.config.from_object(st.config)

# Connect the database
um.db.init_app(server)

# Set up the login manager
login_manager = fl.LoginManager()
login_manager.init_app(server)
login_manager.login_view = '/login'


class User(fl.UserMixin, um.User):
    pass


@login_manager.user_loader
def load_user(user_id):
    """
    Callback to reload the user object
    """
    return User.query.get(int(user_id))

