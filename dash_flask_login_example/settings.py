import os
import pathlib as pl
import dotenv as de

# Load environment variables from .env
ROOT = pl.Path(os.path.abspath(__file__)).parent.parent
de.load_dotenv(ROOT / ".env")

class Config(object):
    """Base configuration."""
    ROOT = ROOT
    APP_DIR = pl.Path(os.path.abspath(__file__)).parent  # This directory
    BCRYPT_LOG_ROUNDS = 13
    DEBUG = False
    DEBUG_TB_ENABLED = False  # Disable Debug toolbar
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    SECRET_KEY = os.urandom(12)
    SQLALCHEMY_DATABASE_URI = f"sqlite:///{ROOT / 'users.db'}"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevConfig(Config):
    """
    Development configuration.
    """
    MODE = "development"
    DEBUG = True
    # Put the db file in project root
    DEBUG_TB_ENABLED = True

class ProdConfig(Config):
    """
    Production configuration.
    """
    MODE = "production"


# Choose configuration from environment variable MODE
mode = os.getenv("MODE")
if mode == "development":
    config = DevConfig
else:
    config = ProdConfig
