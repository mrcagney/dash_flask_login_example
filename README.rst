Dash Flask Login Example
*************************
This is a Python 3.8+ example of using Flask-Login within a Dash application to
authenticate users.
The example uses an SQLite3 database, but you can use a different database by change the
database parameters in ``settings.py``.


Authors
=======
- Alex Raichev, 2021-03


Installation
============
1. Clone the repository.
2. Create a Python 3.8+ virtual environment.
3. Initialize Poetry and install the requirements via ``poetry install``.


Usage
=====
1. Change to the repo's directory.
2. Activate your virtual environment.
3. Once and for all, create the User table in your database via ``python dash_flask_login_example/user_management.py create-user-table``.
4. Add a user to the database via ``python dash_flask_login_example/user_management.py add-user <username> <password> <email address>``.
5. View the other database commands via ``python dash_flask_login_example/user_management.py --help``.
6. Run the application via ``python dash_flask_login_example/index.py``.


Files
=====
- ``settings.py``: Settings for the app. Set your database here.
- ``app.py``: App setup.
- ``index.py``: Index page for the multipage app.
- ``login.py``: Login page.
- ``logout.py``: Logout page.
- ``main.py``: The main page. Has an example graph.
- ``user_management.py``: Database set up plus basic commands for managing users.
"production").
- ``wsgi.py``: WSGI file in case you want to deploy the app to an Apache server.
- ``.env``: The environment file in which you set your app mode ("development" or

Notes
=====
- Video walk through of the code is on Microsoft Teams `here <https://web.microsoftstream.com/video/aa68d1b7-0766-4659-869d-34f262f398fd>`_.
- I created this example to suit my needs and based it on the Github repo `https://github.com/RafaelMiquelino/dash-flask-login <https://github.com/RafaelMiquelino/dash-flask-login>`_.
- For more complex user authentication flow, see the Github repo `https://github.com/russellromney/dash-auth-flow <https://github.com/russellromney/dash-auth-flow>`_.


Changes
=======

1.2.0, 2021-03-10
-----------------
- Fixed logout link to disappear properly.
- Added link to video of code walkthrough.


1.1.0, 2021-03-04
-----------------
- Simplifed routing and added username to main layout.


1.0.0, 2021-03-04
-----------------
- First release.